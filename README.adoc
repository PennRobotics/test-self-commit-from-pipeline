= Test Self-Commit from Pipeline

It is possible to generate files and commit them to ones own repository on
Github. Here is the method of doing this on Gitlab:

== Commit to your own repo

=== Steps

* Generate a new project access token
**  _Project_ &rarr; _Settings_ &rarr; _Access Tokens_ &rarr; _Add new token_
**  _read/write repository_ access plus _Maintainer_ role
* Store the value of the token in a CI/CD variable
**  _Project_ &rarr; _Settings_ &rarr; _CI/CD_ &rarr; _Variables_ &rarr; _Add variable_
**  Key: `OAUTH_TOKEN`
**  Masking
* Create **.gitlab-ci.yml** with contents similar to shown:
**  If you use a different image (for features or size) be aware that you need to figure out how each git dependency is installed and configured. For instance, Alpine Linux uses _Alpine Package Keeper (apk)_
**  Naturally, each line can be modified e.g. commit message and flags, branch name, pipeline source, etc. Perhaps some lines (name/email) could even be populated via built-in variables?

=== Script

[,yml]
----
stages:
  - test

touchandgo:
  stage: test
  image: ubuntu:mantic-20230807.1
  script:
    - apt update
    - apt install --no-install-recommends -y git ca-certificates apt-transport-https
    - update-ca-certificates
    - git remote remove origin
    - git remote add origin https://oauth2:$OAUTH_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git

    - git config user.name "<NAME OF COMMITTER>"
    - git config user.email "<EMAIL ADDRESS OF COMMITTER>"

    - echo THIS IS WHERE YOU CREATE/MODIFY FILES

    # THIS IS WHERE YOU CAN BLINDLY ADD ALL OR SPECIFY FILES:
    - git add .

    - git commit -m "Update repository via pipeline"
    - git push origin HEAD:main

  allow_failure: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: always
----


== Development Process

=== First Attempt

==== Input

[,yml]
----
stages:
  - test

touchandgo:
  stage: test
  image: ubuntu:mantic-20230807.1
  script:
    - apt update
    - apt install -y git
    - mkdir -p testdir
    - touch testdir/emptyfile
    - git config user.name "Brian Wright"
    - git config user.email "10962815-PennRobotics@users.noreply.gitlab.com"
    - git add testdir/emptyfile
    - git commit -m "test commit"
    - git push origin HEAD:main
  allow_failure: true
----

==== Output

----
$ git push origin HEAD:main
remote: You are not allowed to upload code.
fatal: unable to access 'https://gitlab.com/PennRobotics/test-self-commit-from-pipeline.git/': The requested URL returned error: 403
Cleaning up project directory and file based variables 00:00
ERROR: Job failed: exit code 1
----


=== Second Attempt

==== Changes

After reading a https://forum.gitlab.com/t/pipeline-help-remote-you-are-not-allowed-to-upload-code-403-best-practices/83576[Gitlab Forum thread], I generated a new project access token (_Project_ &rarr; _Settings_ &rarr; _Access Tokens_ &rarr; _Add new token_) with _read/write repository_ access plus _Maintainer_ role and stored it in a CI/CD variable (_Project_ &rarr; _Settings_ &rarr; _CI/CD_ &rarr; _Variables_ &rarr; _Add variable_) with key `OAUTH_TOKEN` and masking applied.

(There is a warning that masking a variable will not fully protect it and to consider _external secrets_ and _file type variables_. I don't think these will be necessary here, but I have been wrong before.)

==== Input

[,yml]
----
stages:
  - test

touchandgo:
  stage: test
  image: ubuntu:mantic-20230807.1
  script:
    - apt update
    - apt install --no-install-recommends -y git
    - git remote remove origin
    - git remote add origin https://oauth2:$OAUTH_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git
    - mkdir -p testdir
    - touch testdir/emptyfile
    - git config user.name "Brian Wright"
    - git config user.email "10962815-PennRobotics@users.noreply.gitlab.com"
    - git add testdir/emptyfile
    - git commit -m "test commit"
    - git push origin HEAD:main
  allow_failure: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: always
----

==== Output

----
$ git push origin HEAD:main
fatal: unable to access 'https://gitlab.com/PennRobotics/test-self-commit-from-pipeline.git/': server certificate verification failed. CAfile: none CRLfile: none
Cleaning up project directory and file based variables 00:00
ERROR: Job failed: exit code 1
----

==== Debrief

This looks like the installation of CA certificates isn't happening anymore after adding `--no-install-recommends`.


=== Third (Final) Attempt

==== Ideas

After searching the error from the last attempt, there are more solutions than installing CA certificates, including setting `GIT_SSL_NO_VERIFY`, using `curl-config --ca`, fixing the system clock, running `update-ca-certificates`, and more&hellip;

==== Input

[,yml]
----
stages:
  - test

touchandgo:
  stage: test
  image: ubuntu:mantic-20230807.1
  script:
    - apt update
    - apt install --no-install-recommends -y git ca-certificates apt-transport-https
    - update-ca-certificates
    - git remote remove origin
    - git remote add origin https://oauth2:$OAUTH_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git
    - mkdir -p testdir
    - touch testdir/emptyfile
    - git config user.name "Brian Wright"
    - git config user.email "10962815-PennRobotics@users.noreply.gitlab.com"
    - git add testdir/emptyfile
    - git commit -m "test commit"
    - git push origin HEAD:main
  allow_failure: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: always
----

==== Output

----
$ git push origin HEAD:main
To https://gitlab.com/PennRobotics/test-self-commit-from-pipeline.git
   7357122..485e350  HEAD -> main
Cleaning up project directory and file based variables 00:00
Job succeeded
----


=== Checklist

* [x] Generate **.gitlab-ci.yml**
* [x] A script should run `touch`, `git add`, `git commit`, `git push`
* [x] Is there already a token accessible during the pipeline? **Answer: not preconfigured**
* [x] If so, does this need to be enabled?
* [ ] Is there a method of listing the CI context, variables, etc.?

_While the last point is interesting generally, it isn't necessary anymore because the final version of the script had the intended outcome._
